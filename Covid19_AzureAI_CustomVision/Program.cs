﻿using System;

using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Covid19_AzureAI_CustomVision
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Welcome to Covid-19 Solution");

            
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Prediction-Key", "Prediction Key");
            //  string imagePath = @"D:\observations-master\observations-master\experiements\data\with_mask\1-with-mask.jpg"; 
            string imagePath = @"Local disc path ";
            byte[] byteData = ConvertImageToByteArray(imagePath);           
            var content = new ByteArrayContent(byteData);
            var url = "Custom Vision API Image URL";
            client.BaseAddress = new Uri(url);

            var response = await client.PostAsync(url, content);
            var responseString = JObject.Parse(await response.Content.ReadAsStringAsync());
            var properties = responseString.Children().Select(ch => (JProperty)ch).ToArray();
            var predictions = properties.FirstOrDefault(p => p.Name.Contains("prediction"));
          
            string ismask = "False";
            var allvalues = predictions.Value.ToArray();
            Response rp = new Response();
            foreach (var prediction in allvalues)
            {
               
                decimal val = (decimal)(prediction["probability"]);            
                if (val > .75M)
                {
                    if ((string)(prediction["tagName"]) == "Mask")
                    {
                        ismask = "True";
                        rp.Ismask = ismask;
                        rp.Prediction = val;
                        break;
                    }
                }
                else
                {
                    rp.Ismask = ismask;
                    rp.Prediction = val;
                }

            }

            Console.WriteLine("The image has the prediction value {0} and mask status is {1} ", rp.Prediction, rp.Ismask);
            Console.ReadLine();

        }
        public static byte[] ConvertImageToByteArray(string imagePath)
        {
            byte[] imageByteArray = null;
            FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            using (BinaryReader reader = new BinaryReader(fileStream))
            {
                imageByteArray = new byte[reader.BaseStream.Length];

                for (int i = 0; i < reader.BaseStream.Length; i++)
                    imageByteArray[i] = reader.ReadByte();
            }

            return imageByteArray;
        }


        class Response
        {
            public string Ismask { get; set; }
            public decimal Prediction { get; set; }
        }

    }
}
